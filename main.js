import Vue from 'vue'
import App from './App'
import {
  task_completeTask
} from "./api/task.js";
import {
	http
} from '@/js_sdk/luch-request/index.js' // 全局挂载引入，配置相关在该index.js文件里修改
import store from './store'
const updateManager = uni.getUpdateManager();
updateManager.onCheckForUpdate(function (res) {
  // 请求完新版本信息的回调
  console.log('hasUpdatehasUpdate',res.hasUpdate);
});

updateManager.onUpdateReady(function (res) {
  uni.showModal({
    title: '更新提示',
    content: '新版本已经准备好，是否重启应用？',
    success(res) {
      if (res.confirm) {
        // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
        updateManager.applyUpdate();
      }
    }
  });

});

updateManager.onUpdateFailed(function (res) {
  // 新的版本下载失败
});
Vue.config.productionTip = false
Vue.prototype.$finish_taskdata = async function (taskIndex, userId) {
  var reqdata = {
    TaskIndex: taskIndex,
    userid: userId
  };
  var resdata = await task_completeTask(reqdata);
  console.log(resdata);
  if (resdata.code == 200) {
    // this.$notify({
    //   title: "任务成功",
    //   message: resdata.text,
    //   type: "success"
    // });
	uni.showToast({
		 title: resdata.text,
		 duration: 2000,
		 icon:"none"
	});
  } else {
    if (taskIndex == 0) {
		uni.showToast({
			 title: resdata.text,
			 duration: 2000,
			 icon:"none"
		});
      // this.$notify({
      //   title: "提醒"+resdata.text,
      //   message: ,
      //   type: "warning"
      // });
    }
  }
};
App.mpType = 'app'
const app = new Vue({
	store,
    ...App
})
app.$mount()
