import { http } from '@/js_sdk/luch-request/index.js'

function request (url, method, postData, hideLoading) {
	//接口请求
	if (hideLoading) {
	  uni.showLoading({
		mask: true,
		title: '请稍候...'
	  })
	}
	return new Promise((resolve, reject) => {
		http[method](url, postData).then(res => {
			setTimeout(() => {
				hideLoading && uni.hideLoading()
			},1000)
			resolve(res.data)
		}).catch(err => {
			if (hideLoading) {
				toast("网络不给力，请稍后再试~")
			}
			reject(err)
		})
	})
}

function toast (text, duration, success) {
	uni.showToast({
	  title: text,
	  icon: success ? 'success' : 'none',
	  duration: duration || 2000
	})
}

export {
  request
}