import { request } from '@/js_sdk/request.js'

export function get_draft(data) {
  return request('/draft', 'get', {params: data}, false)
}

export function post_draft(data) {
  return request('/draft', 'post', data, false)
}

export function put_draft(data) {
  return request('/draft', 'put', data, false)
}

export function delete_draft(data) {
  return request('/draft', 'delete', data, false)
}