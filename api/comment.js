import { request } from '@/js_sdk/request.js'

//例如: data = {userName: 'name', password: '123456'}
//传参格式 ↑

export function getComment(data) {
  return request('/getComment', 'post', data, true)
}
export function get_comment(data) {
  return request('/comment', 'get', {params: data}, false)
}

export function post_comment(data) {
  return request('/comment', 'post', data, false)
}

export function put_comment(data) {
  return request('/comment', 'put', data, false)
}

export function delete_comment(data) {
  return request('/comment', 'delete', data, false)
}

export function delectFistComment(data) {
  return request('/delectFistComment', 'post', data, false)
}