import { request } from '@/js_sdk/request.js'
 export function get_departmentlabel(data) {
  return request('/departmentlabel', 'get', {params: data} , false)
}