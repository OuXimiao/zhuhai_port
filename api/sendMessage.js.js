import { request } from '@/js_sdk/request.js'
 export function sendMessage(data) {
  return request('/sendMessage', 'post', data, false)
}