import { request } from '@/js_sdk/request.js'

export function get_user_login(data) {
  return request('/user_login', 'get', {params: data}, false)
}

export function post_user_login(data) {
  return request('/user_login', 'post', data, true)
}

export function put_user_login(data) {
  return request('/user_login', 'put', data, true)
}

export function delete_user_login(data) {
  return request('/user_login', 'delete', data, true)
}