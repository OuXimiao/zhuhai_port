import { request } from '@/js_sdk/request.js'
export function getproposal(data) {
  return request('/getproposal', 'post', data, false)
}

export function get_problem(data) {
  return request('/problem', 'get', {params: data}, false)
}

export function post_problem(data) {
  return request('/problem', 'post', data, false)
}

export function put_problem(data) {
  return request('/problem', 'put', data, false)
}

export function delete_problem(data) {
  return request('/problem', 'delete', data, false)
}