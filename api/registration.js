import { request } from '@/js_sdk/request.js'
export function registration(data) {
  return request('/registration', 'post', data, true)
}
