import { request } from '@/js_sdk/request.js'
 export function exchangeGoods(data) {
  return request('/exchangeGoods', 'post', data, true)
}