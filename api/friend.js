import { request } from '@/js_sdk/request.js'

export function get_friend(data) {
  return request('/friend', 'get', {params: data}, false)
}

export function post_friend(data) {
  return request('/friend', 'post', data, true)
}

export function put_friend(data) {
  return request('/friend', 'put', data, false)
}

export function delete_friend(data) {
  return request('/friend', 'delete', data, true)
}