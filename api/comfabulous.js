import { request } from '@/js_sdk/request.js'

export function get_comfabulous(data) {
  return request('/comfabulous', 'get', {params: data}, false)
}

export function post_comfabulous(data) {
  return request('/comfabulous', 'post', data, false)
}

export function put_comfabulous(data) {
  return request('/comfabulous', 'put', data, false)
}

export function delete_comfabulous(data) {
  return request('/comfabulous', 'delete', data, false)
}