import { request } from '@/js_sdk/request.js'

export function get_completeTask(data) {
  return request('/completeTask', 'get', {params: data}, false)
}

export function post_completeTask(data) {
  return request('/completeTask', 'post', data, false)
}

export function put_completeTask(data) {
  return request('/completeTask', 'put', data, false)
}

export function delete_completeTask(data) {
  return request('/completeTask', 'delete', data, false)
}