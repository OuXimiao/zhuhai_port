import { request } from '@/js_sdk/request.js'
export function postvote(data) {
  return request('/vote', 'post', data, false)
}
export function get_vote(data) {
  return request('/vote', 'get', {params: data}, false)
}

export function put_vote(data) {
  return request('/vote', 'put', data, false)
}


export function postvoteDetail(data) {
  return request('/voteDetail', 'post', data, false)
}
export function get_voteDetail(data) {
  return request('/voteDetail', 'get', {params: data}, false)
}

export function put_voteDetail(data) {
  return request('/voteDetail', 'put', data, false)
}

export function voteimg(data) {
  return request('/voteimg', 'post', data, false)
}
export function delete_vote(data) {
  return request('/put_voteImageTests', 'post', data, false)
}