import { request } from '@/js_sdk/request.js'

export function getfollowart(data) {
  return request('/getFollowArt', 'post',data, false)
}

export function get_follow(data) {
  return request('/follow', 'get', {params: data}, false)
}

export function post_follow(data) {
  return request('/follow', 'post', data, false)
}

export function put_follow(data) {
  return request('/follow', 'put', data, false)
}

export function delete_follow(data) {
  return request('/follow', 'delete', data, false)
}