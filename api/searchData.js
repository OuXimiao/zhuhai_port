import { request } from '@/js_sdk/request.js'

//例如: data = {userName: 'name', password: '123456'}
//传参格式 ↑
export function post_searchData(data) {
   return request('/searchData', 'post', data, false)
}