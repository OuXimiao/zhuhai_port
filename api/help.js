import { request } from '@/js_sdk/request.js'

export function get_help(data) {
  return request('/help', 'get', {params: data}, false)
}

export function post_help(data) {
  return request('/help', 'post', data, false)
}

export function put_help(data) {
  return request('/help', 'put', data, false)
}

export function delete_help(data) {
  return request('/help', 'delete', data, false)
}