import { request } from '@/js_sdk/request.js'

export function get_like(data) {
  return request('/like', 'get', {params: data}, false)
}

export function post_like(data) {
  return request('/like', 'post', data, false)
}

export function put_like(data) {
  return request('/like', 'put', data, false)
}

export function delete_like(data) {
  return request('/like', 'delete', data, false)
}