import { request } from '@/js_sdk/request.js'
 export function get_commodity(data) {
  return request('/commodity', 'get', {params: data}, false)
}
