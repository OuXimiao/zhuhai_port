import { request } from '@/js_sdk/request.js'

//例如: data = {userName: 'name', password: '123456'}
//传参格式 ↑

export function deleimg(data) {
  return request('/deleimg', 'post', data, true)
}

export function deleencl(data) {
  return request('/deleencl', 'post', data, true)
}
