import { request } from '@/js_sdk/request.js'
 export function get_label(data) {
  return request('/label', 'get', {params: data} , false)
}
