import { request } from '@/js_sdk/request.js'
export function getAnswers(data) {
  return request('/getAnswers', 'post', data, false)
}
export function get_answer(data) {
  return request('/answer', 'get', {params: data}, false)
}
export function post_answer(data) {
  return request('/answer', 'post', data, false)
}
export function put_answer(data) {
  return request('/answer', 'put', data, false)
}