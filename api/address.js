import { request } from '@/js_sdk/request.js'
 export function get_address(data) {
  return request('/addressdata', 'get', {params: data}, false)
}