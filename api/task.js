import { request } from '@/js_sdk/request.js'

//例如: data = {userName: 'name', password: '123456'}
//传参格式 ↑

export function get_task(data) {
   return request('/task', 'get', {params: data}, false)
}
export function task_completeTask(data) {
   return request('/Task_completeTask', 'post', data, false)
}