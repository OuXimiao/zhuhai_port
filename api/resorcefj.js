import { request } from '@/js_sdk/request.js'

//例如: data = {userName: 'name', password: '123456'}
//传参格式 ↑

export function post_resorcefj(data) {
  return request('/resorcefj', 'post', data, true)
}
export function get_resource(data) {
  return request('/resource', 'get', {params: data}, false)
}

export function post_resource(data) {
  return request('/resource', 'post', data, false)
}

export function put_resource(data) {
  return request('/resource', 'put', data, false)
}

export function delete_resource(data) {
  return request('/resource', 'delete', data, false)
}