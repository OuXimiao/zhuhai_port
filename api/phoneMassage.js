import { request } from '@/js_sdk/request.js'
 export function PhoneMessage(data) {
  return request('/PhoneMessage', 'post', data, true)
}