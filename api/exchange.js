import { request } from '@/js_sdk/request.js'

//例如: data = {userName: 'name', password: '123456'}
//传参格式 ↑

export function get_exchange(data) {
   return request('/exchange', 'get', {params: data}, false)
}