import { request } from '@/js_sdk/request.js'
 export function forgetPwd(data) {
  return request('/forgetPwd', 'post', data, false)
}
