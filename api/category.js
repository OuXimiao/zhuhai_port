import { request } from '@/js_sdk/request.js'
 export function get_category(data) {
  return request('/category', 'get', {params: data}, false)
}
