import { request } from '@/js_sdk/request.js'
 export function login(data) {
  return request('/login', 'post', data, true)
}