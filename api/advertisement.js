import { request } from '@/js_sdk/request.js'

export function get_Advertisement(data) {
  return request('/Advertisement', 'get', {params: data}, false)
}
export function post_Advertisement(data) {
  return request('/Advertisement', 'post', data, false)
}
export function put_Advertisement(data) {
  return request('/Advertisement', 'put', data, false)
}