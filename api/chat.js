import { request } from '@/js_sdk/request.js'

export function get_chat(data) {
  return request('/chat', 'get', {params: data}, false)
}

export function post_chat(data) {
  return request('/chat', 'post', data, true)
}

export function put_chat(data) {
  return request('/chat', 'put', data, true)
}

export function delete_chat(data) {
  return request('/chat', 'delete', data, true)
}