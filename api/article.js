import { request } from '@/js_sdk/request.js'
export function postarticle(data) {
  return request('/article', 'post', data, false)
}
export function get_article(data) {
  return request('/article', 'get', {params: data}, false)
}

export function put_article(data) {
  return request('/article', 'put', data, false)
}