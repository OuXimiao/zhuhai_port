import { request } from '@/js_sdk/request.js'
export function postquestion(data) {
  return request('/question', 'post', data, false)
}
export function get_question(data) {
  return request('/question', 'get', {params: data}, false)
}

export function put_question(data) {
  return request('/question', 'put', data, false)
}

export function postquestion_reply(data) {
  return request('/question_reply', 'post', data, false)
}
export function getquestion_reply(data) {
  return request('/question_reply', 'get', {params: data}, false)
}

export function putquestion_reply(data) {
  return request('/question_reply', 'put', data, false)
}
export function delete_question(data) {
  return request('/put_questionImageTests', 'post', data, false)
}