import { request } from '@/js_sdk/request.js'

//删除图片
export function post_delectImg(data) {
  return request("/delectImg","post",data,false);
}

//删除资源
export function post_enclosure(data) {
  return request("/enclosure","post",data, false);
}

//删除文章（珠海港圈,创新建议）
export function post_delect_article(data) {
  return request("/delectArticle","post",data, false);
}

//删除文章（资源共享）
export function post_delect_resource(data) {
  return request("/delectResource","post",data, false);
}
//删除文章（资源共享）
export function post_delect_answer(data) {
  return request("/delectAnswer","post",data, false);
}