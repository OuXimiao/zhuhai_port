import { request } from '@/js_sdk/request.js'

export function get_history(data) {
  return request('/history', 'get', {params: data}, false)
}

export function post_history(data) {
  return request('/history', 'post', data, false)
}

export function put_history(data) {
  return request('/history', 'put', data, false)
}

export function delete_history(data) {
  return request('/history', 'delete', data, false)
}