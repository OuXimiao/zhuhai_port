import Vue from "vue";
import Vuex from "vuex";
import { login } from "@/api/login";
// import {
//   SET_StorageTOKEN,
//   REMOVE_StorageTOKEN
// } from "@/js_sdk/utils/storageToken.js";
// import { REMOVE_StorageUserInfo } from  "@/js_sdk/utils/storageUserInfo.js";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
      token:"",
	  userInfo:{},
	  accessToken:"",
	  gzhurl:''
	  },
  mutations: {
    SET_USERINFO: (state, userInfo) => {
      state.userInfo = userInfo;
    },
    SET_TOKEN:(state,token)=>{
	  state.token = token;
	},
	SET_ACCESSTOKEN:(state,accessToken)=>{
	  state.accessToken = accessToken;
	},
	SET_GZHURL:(state,gzhurl)=>{
		state.gzhurl = gzhurl;
	}
  },
  actions: {
    // 登录
      Login({ commit, state }, userInfo) {
        return new Promise(async (resolve, reject) => {
          try {
            login(userInfo).then(res => {
				 resolve(res);
            }).catch((err) => {
    		   console.log(err,"请求错误")
    	   });
          } catch (e) {
            reject(e);
          }
        });
      },
      // 登出
    //   LogOut({ commit, state }) {
    //     return new Promise((resolve, reject) => {
    // commit("SET_USERINFO", "");
    // commit("SET_OPENID", "");
    // commit("SET_TOKEN", "");
    // REMOVE_StorageTOKEN();
    // REMOVE_StorageUserInfo();
    //     });
    //   },
  },
  getters: {
    userInfo:state => state.userInfo,
    token:state => state.token,
	accessToken:state => state.accessToken,
	gzhurl:state => state.gzhurl
  }
});

export default store;
