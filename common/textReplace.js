const textReplace = (htmlcon, size = 200) => {
  var str = htmlcon; //html文字字符串
  var con = str.replace(/\s*/g, ""); //去掉空格
  var res = con.replace(/<[^>]+>/g, ""); //去掉所有的html标记
  var res1 = res.replace(/↵/g, ""); //去掉所有的↵符号
  var res2 = res1.replace(/[\r\n]/g, ""); //去掉回车换行
  var res3 = res2.replace(/&nbsp;/g, ""); //去掉回车换行
  var res4 = res3.substring(0, size);
  return res4;
};

module.exports = {
  textReplace
};